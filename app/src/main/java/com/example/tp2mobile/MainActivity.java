package com.example.tp2mobile;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final WineDbHelper dbHelper = new WineDbHelper(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                startActivity(intent);
            }
        });

        if (!dbHelper.HasValues()){
            dbHelper.populate();
        }
        Cursor cursor = dbHelper.fetchAllWines();
        String[] values = new String[cursor.getCount()];
        for (int i=0; i<cursor.getCount();i++){
            values[i] = cursor.getString(cursor.getColumnIndex(dbHelper.COLUMN_NAME));
            cursor.moveToNext();
        }

        final ListView listView = (ListView)findViewById(R.id.wineList);
        final ArrayAdapter<String>  adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, values);
        listView.setAdapter(adapter);
    }


    @Override
    protected void onResume() {
        super.onResume();
        WineDbHelper dbHelper = new WineDbHelper(this);
        Cursor cursor = dbHelper.fetchAllWines();
        String[] values = new String[cursor.getCount()];
        for (int i=0; i<cursor.getCount();i++){
            values[i] = cursor.getString(cursor.getColumnIndex(dbHelper.COLUMN_NAME));
            cursor.moveToNext();
        }

        final ListView listView = (ListView)findViewById(R.id.wineList);
        final ArrayAdapter<String>  adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, values);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
