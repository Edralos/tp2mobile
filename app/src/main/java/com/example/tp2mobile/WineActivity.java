package com.example.tp2mobile;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class WineActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);
        Intent intent = getIntent();
        Button saveButton = (Button) findViewById(R.id.button);

        final TextView nameView = (TextView)findViewById(R.id.wineName);
        final TextView localisationView = (TextView)findViewById(R.id.editLoc);
        final TextView regionView = (TextView)findViewById(R.id.editWineRegion);
        final TextView climatView = (TextView)findViewById(R.id.editClimate);
        final TextView surface_planteeView = (TextView)findViewById(R.id.editPlantedArea);
        final WineDbHelper db = new WineDbHelper(this);




        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameView.getText().toString();
                String loc = localisationView.getText().toString();
                String region = regionView.getText().toString();
                String climat = climatView.getText().toString();
                String surface = surface_planteeView.getText().toString();
                Wine newWine = new Wine(name,region,loc,climat,surface);
                db.addWine(newWine);
                Log.i("adding wine", newWine.getTitle());
                finish();
            }
        });

    }



}
